﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RESTFull.Infrastructure;
using RESTFull.Infrastructure.Repository;
using RESTFull.Domain;
using RESTFull.Infrastructure;

namespace WebAPI01.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class legalController : ControllerBase
    {
        private readonly Context _context;
        private readonly LegalEntityRepository _legalRepository;
        public legalController(Context context)
        {
            _context = context;
            _legalRepository = new LegalEntityRepository(_context);
        }

        // GET: api/legal
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LegalEntity>>> getEntities()
        {
            return await _legalRepository.getAll();
        }

        // GET: api/legal/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<LegalEntity>> getEntityById(int id)
        {
            //var person = await _context.Persons.FindAsync(id);
            var legalEntity = await _legalRepository.getById(id);
            if (legalEntity == null)
            {
                return NotFound();
            }
            return legalEntity;
        }
        
        // GET: api/legal/Yandex
        [HttpGet("{name}")]
        public async Task<ActionResult<LegalEntity>> GetEntityByName(String name)
        {
            //var person = await _context.Persons.FindAsync(id);
            var legalEntity = await _legalRepository.getByName(name);
            if (legalEntity == null)
            {
                return NotFound();
            }
            return legalEntity;
        }
        

        // PUT: api/legal/5
        [HttpPut("{id}")]
        // public async Task<IActionResult> putEntity(int id, LegalEntity person)
        public async Task<IActionResult> putEntity(LegalEntity entity)
        {
            // if (id != person.id)
            // {
                // return BadRequest();
            // }

            await _legalRepository.update(entity);

            return NoContent();
        }

        // POST: api/legal
        [HttpPost]
        public async Task<ActionResult<LegalEntity>> postEntity(LegalEntity entity)
        {
            Console.WriteLine("Find me");
            Console.WriteLine("Adding person");
            await _legalRepository.add(entity);
            return CreatedAtAction("getEntityById", new { id = entity.id }, entity);
        }
        
        // [HttpPost]
        // public async Task<ActionResult<LegalEntity>> postEntity(Dictionary<string, string> map)
        // {
            // Console.WriteLine("Find me");
            // Console.WriteLine("Adding person");
            
            // var entity = new LegalEntity
            // {
                // name = map["name"],
                // address = map["address"],
                // ceo = map["ceo"],
                // fond = long.Parse(map["fond"]),
                // Url = ""
            // };

            // await _legalRepository.add(entity);
            // return CreatedAtAction("getEntityById", new { id = entity.id }, entity);
        // }

        // DELETE: api/legal/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> deleteEntity(int id)
        {
            //var person = await _context.Persons.FindAsync(id);
            var entity = await _legalRepository.getById(id);
            if (entity == null)
            {
                return NotFound();
            }

            //_context.Persons.Remove(person);
            //await _context.SaveChangesAsync();
            await _legalRepository.delete(id);

            return NoContent();
        }

        private bool entityExists(int id)
        {
            return _context.legal_entities.Any(e => e.id == id);
        }
    }

    [Route("api/test/[controller]")]
    [ApiController]
    public class testController : ControllerBase
    {
        [HttpPost]
        public void postMember(Member member)
        {
            Console.WriteLine("Find me");
            Console.WriteLine("Got member!");
            Console.WriteLine(member.fullName);
            // return CreatedAtAction("getEntityById", new { id = member.id }, member);
        }
        
    }
}
