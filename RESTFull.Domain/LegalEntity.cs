namespace RESTFull.Domain;

public class LegalEntity
{
    public List<Member> members { get; set; } = new List<Member>();

    public int id { get; set; }
    public string name { get; set; }
    public string address { get; set; }
    public long fond { get; set; }
    public string ceo { get; set; }
    public DateTime registrationDate { get; set; }

    public string Url { get; set; }
}