namespace RESTFull.Domain;

public class Member
{
    public int id { get; set; }
    public string fullName { get; set; }

    public int legalEntityId { get; set; }
}