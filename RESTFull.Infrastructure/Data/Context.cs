﻿using Microsoft.EntityFrameworkCore;
using RESTFull.Domain;

namespace RESTFull.Infrastructure;

public class Context : DbContext
{
    public Context()
    {
    }

    public Context(DbContextOptions<Context> options) : base(options)
    {
    }

    public DbSet<LegalEntity> legal_entities { get; set; }

    public DbSet<Member> members { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
    }
}