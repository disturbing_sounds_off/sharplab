using Microsoft.EntityFrameworkCore;
using RESTFull.Domain;

namespace RESTFull.Infrastructure.Repository;

public class LegalEntityRepository
{
    private readonly Context _context;

    public LegalEntityRepository(Context context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public Context getContext()
    {
        return _context;
    }

    public async Task<List<LegalEntity>> getAll()
    {
        return await _context.legal_entities.OrderBy(e => e.name)
            .AsNoTracking()
            .ToListAsync();
    }
    
    public async Task<LegalEntity?> getById(int id)
    {
        // return await _context.legal_entities.FindAsync(id);
        return await _context.legal_entities.Where(e => e.id == id)
        .Include(e => e.members)
        .FirstOrDefaultAsync();
    }

    public async Task<LegalEntity?> getByName(string name)
    {
        return await _context.legal_entities.Where(e => e.name == name)
            .Include(e => e.members)
            .FirstOrDefaultAsync();
    }


    public async Task add(LegalEntity entity)
    {
        _context.legal_entities.Add(entity);
        await _context.SaveChangesAsync();
    }

    public async Task delete(int id)
    {
        var entity = await _context.legal_entities.FindAsync(id);
        if (entity != null)
        {
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }

    public void changeTrackerClear()
    {
        _context.ChangeTracker.Clear();
    }

    public async Task update(LegalEntity entity)
    {
        var entityInDb = getById(entity.id).Result;

        if (entityInDb != null)
        {
            
            _context.Entry(entityInDb).CurrentValues.SetValues(entity);
            foreach (var member in entity.members)
            {
                var memberInDb = entityInDb.members.FirstOrDefault(m => m.id == member.id);
                if (memberInDb == null)
                    entityInDb.members.Add(member);
                else
                {
                    _context.Entry(memberInDb).CurrentValues.SetValues(member);
                }
            }

            foreach (var memberInDb in entityInDb.members)
                if (!entity.members.Any(m => m.id == memberInDb.id))
                    _context.Remove(memberInDb);
        }

        await _context.SaveChangesAsync();
    }
}