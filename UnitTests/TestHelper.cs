using Microsoft.EntityFrameworkCore;
using RESTFull.Domain;
using RESTFull.Infrastructure;
using RESTFull.Infrastructure.Repository;

namespace UnitTests;

public class TestHelper
{
    private readonly Context _context;

    public TestHelper()
    {
        var contextOptions = new DbContextOptionsBuilder<Context>()
            .UseNpgsql("Server=localhost;Database=legal_entity;Port=5432;User Id=postgres;Password=")
            .Options;
        
        _context = new Context(contextOptions);

        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();

        var entity1 = new LegalEntity
        {
            // id = 10,
            name = "NCIX",
            ceo = "Larionov Oleg Sergeevich",
            address = "13720 Mayfield Place Richmond, BC, Canada",
            Url = "",
        };
        entity1.members.Add(new Member{fullName = "Ivanov Nikolai Arcadievich"});
        entity1.members.Add(new Member{fullName = "Nekrasova Sophia Michailovna"});

        _context.legal_entities.Add(entity1);
        
        _context.SaveChanges();
        _context.ChangeTracker.Clear();
        
        var entity2 = new LegalEntity
        {
            // id = 20,
            name = "Yandex",
            ceo = "Oleg Sergeevich Larionov",
            address = "Moscow, Russia",
            Url = "",
        };
        entity2.members.Add(new Member{fullName = "Nikolai Arcadievich Ivanov"});
        entity2.members.Add(new Member{fullName = "Sophia Michailovna Nekrasova"});

        _context.legal_entities.Add(entity2);
        
        _context.SaveChanges();
        _context.ChangeTracker.Clear();
    }

    public LegalEntityRepository LegalEntityRepository
    {
        get
        {
            return new LegalEntityRepository(_context);
        }
    }
}