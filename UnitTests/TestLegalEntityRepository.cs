using RESTFull.Domain;
using Xunit;

namespace UnitTests;

public class TestLegalEntityRepository
{
    [Fact]
    public void voidTest()
    {
        var testHelper = new TestHelper();
        var entityRepository = testHelper.LegalEntityRepository;
        
        Assert.Equal(1,1);
    }

    [Fact]
    public void testAdd()
    {
        var testHelper = new TestHelper();
        var entityRepository = testHelper.LegalEntityRepository;
        var entity = new LegalEntity
        {
            // id = 20,
            name = "BurgerKing", 
            ceo = "Larionov Oleg Sergeevich",
            address = "Санкт-Петербург, ул. Дыбенко, 27, корп. 4",
            Url = "",
        };

        entityRepository.add(entity).Wait();
        entityRepository.changeTrackerClear();
        
        Assert.True(entityRepository.getAll().Result.Count > 0);
        Assert.True(entityRepository.getAll().Result.Count == 3);
        Assert.Equal("NCIX", entityRepository.getByName("NCIX").Result.name);
        Assert.Equal("BurgerKing", entityRepository.getByName("BurgerKing").Result.name);
        Assert.Equal("Larionov Oleg Sergeevich" , entityRepository.getByName("BurgerKing").Result.ceo);
        Assert.Equal(2, entityRepository.getByName("NCIX").Result.members.Count);
    }

    [Fact]
    public void testUpdateAdd()
    {
        var testHelper = new TestHelper();
        var entityRepository = testHelper.LegalEntityRepository;
        var entity = entityRepository.getByName("NCIX").Result;

        entityRepository.changeTrackerClear();
        entity.name = "Google";
        var member = new Member { fullName = "Larionov Artem Sergeevich" };
        entity.members.Add(member);

        entityRepository.update(entity).Wait();
        
        Assert.Equal("Google", entityRepository.getByName("Google").Result.name);
        Assert.Equal(3, entityRepository.getByName("Google").Result.members.Count);
    }

    [Fact]
    public void testUpdateDelete()
    {
        var testHelper = new TestHelper();
        var entityRepository = testHelper.LegalEntityRepository;
        var entity = entityRepository.getByName("NCIX").Result;
        
        entityRepository.changeTrackerClear();
        entity.members.RemoveAt(0);
        entityRepository.update(entity).Wait();
        
        Assert.Equal(1, entityRepository.getByName("NCIX").Result.members.Count);
    }
}